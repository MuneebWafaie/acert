import { dispatcher } from '../InternalTree'
// import { logger, LogLevel } from '../Logger'

export function useState<T> (defaultValue:T) {
  if (dispatcher.currentDispatcher === null) throw new Error('called useState while no component is being rendered(probably outside a component)')
  // logger(LogLevel.Info, '[hooks]', 'Core/hooks.ts > useState', 'defaultValue', defaultValue, 'dispatcher', dispatcher.currentDispatcher)
  return dispatcher.currentDispatcher.useState(defaultValue)
}

export function useEffect (effect: (()=>void | (()=>()=>void)), deps?: any[]) {
  if (dispatcher.currentDispatcher === null) throw new Error('called useState while no component is being rendered(probably outside a component)')
  // logger(LogLevel.Info, '[hooks]', 'Core/hooks.ts > useEffect', 'effect', effect, 'deps', deps, 'dispatcher', dispatcher.currentDispatcher)
  return dispatcher.currentDispatcher.useEffect(effect, deps)
}

export function useRef<T> (initialValue: T) {
  if (dispatcher.currentDispatcher === null) throw new Error('called useState while no component is being rendered(probably outside a component)')
  // logger(LogLevel.Info, '[hooks]', 'Core/hooks.ts > useEffect', 'effect', effect, 'deps', deps, 'dispatcher', dispatcher.currentDispatcher)
  return dispatcher.currentDispatcher.useRef(initialValue)
}
