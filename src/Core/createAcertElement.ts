import { Component, Props, AcertNode, AcertElement, AcertChild } from '../types'
/**
 * Creates an AcertElement
 * @param type - what the returned AcertElement should describe.
 * @param props - properties of the returned AcertElement, passed to component as a single argument during rendering.
 * @param children - children of the returned AcertElement.
 * @returns an AcertElement
 */
export function createAcertElement (type: string | Component, props: Props, ...children: AcertChild[]):AcertElement {
  if (children.length > 0) {
    props = props || {}
    props.children = children.length > 1 ? children : children[0]
  }

  // otherwise typescript complains
  if (typeof type === 'string') return { type, props } // return HostAcertElement
  return { type, props } // return CompositeAcertElement
}
