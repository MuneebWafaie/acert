import { Attributes, Element as XmlElement } from 'xml-js'
import { Props, AcertChild } from '../../types'
import { createAcertElement } from '../createAcertElement'
import { isAcertNode } from '../utils'

export function getAcertChildFromXmlNode (xmlElement: XmlElement, substitutions: any[]): AcertChild {
  // logger(LogLevel.Info, '[acertxml]', 'Core/acertxml/utils.ts > getAcertNodeFromXmlNode', 'converting xmlNode', xmlElement, 'and substitutions', substitutions, 'to AcertNode')
  const xmlChildren = xmlElement.elements || []

  // tldr: not root node; is an xml element node(rather then text node) and has name(the root node is an element node but does not have a name)
  if (xmlElement.type === 'element' && xmlElement.name) return getAcertChildFromXmlElement(xmlElement, substitutions)

  // is text node
  if (xmlElement.type === 'text') return String(xmlElement.text)

  // is root node
  return getAcertChildFromXmlNode(xmlChildren[0], substitutions)
}

function getAcertChildFromXmlElement (xmlElement: XmlElement, substitutions: any[]): AcertChild {
  // logger(LogLevel.Info, '[acertxml]', 'Core/acertxml/utils.ts > getAcertNodeFromXmlElement', 'converting xmlElement', xmlElement, 'and substitutions', substitutions, 'to AcertNode')
  const xmlElementName = xmlElement.name
  const xmlAttributes = xmlElement.attributes
  const xmlChildren = xmlElement.elements || []
  if (xmlElement.type !== 'element' || !xmlElementName) throw new Error('expected an xml element(tag)')

  let props: Props = null
  if (xmlAttributes) props = getPropsFromAttributes(xmlAttributes, substitutions)
  const substitute = getSubstituteValue(xmlElementName, substitutions) // get xml name substitue

  // substitue value is an AcertNode
  if (isAcertNode(substitute) || Array.isArray(substitute)) return substitute

  // substitue value cannot be converted into AcertElement(subset of AcertNode)
  if (typeof substitute !== 'function' && typeof substitute !== 'string') throw new Error(`${typeof substitute} cannot be converted into AcertNode`)

  const childAcertNodes = xmlChildren.map(child => getAcertChildFromXmlNode(child, substitutions))
  return createAcertElement(substitute, props, ...childAcertNodes)
}

function getPropsFromAttributes (attributes: Attributes, substitutions: any[]) {
  const props:Props = {}
  Object.keys(attributes)
    .filter(attributeName => Object.prototype.hasOwnProperty.call(attributes, attributeName))
    .forEach(attributeName => {
      const attribute = attributes[attributeName]
      props[attributeName] = attribute && getSubstituteValue(attribute, substitutions)
    })
  return props
}

function getSubstituteValue (value:string|number, substitutions:any[]) {
  if (typeof value !== 'string') return value
  if (value.slice(0, 2) === '::') {
    let idx:string | number = value.slice(2)
    if (idx === '' || isNaN(Number(idx))) return value
    idx = Number(idx)
    return substitutions[idx]
  }
  return value
}
