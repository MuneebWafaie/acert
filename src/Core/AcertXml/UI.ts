import { xml2js, Element as XmlElement } from 'xml-js'
import { AcertNode } from '../../types'
import { getAcertChildFromXmlNode } from './utils'

/**
 * Converts Acert xml into AcertNode
 */
export function UI (strings: TemplateStringsArray, ...substitutions:any[]):AcertNode {
  // logger(LogLevel.Info, '[acertxml]', 'Core/acertxml/UI.ts > UI', 'converting strings', strings, 'and substitutions', substitutions, 'to AcertNode')
  const xml = substitutions.reduce(
    (acc, substitution, idx) => acc.slice(-2) === '::' ? acc + getRefFirstIndex(substitutions, substitution) + strings[idx + 1] : acc + String(substitution) + strings[idx + 1],
    strings[0]
  )

  const parsedXml = xml2js(xml, { compact: false }) as XmlElement // OGTYPE: XmlElement | ElementCompact AS: since compact is false can't be ElementCompact
  return (getAcertChildFromXmlNode(parsedXml, substitutions) as AcertNode) // OGTYPE: dont know why
}

function getRefFirstIndex (array: any[], ref:any): number {
  return array.findIndex(el => Object.is(ref, el))
}
