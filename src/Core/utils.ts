export function isAcertNode (value: any) {
  return typeof value === 'number' || typeof value === 'boolean' || typeof value === 'undefined' || value === null || isAcertElement(value)
}

export function isAcertHostElement (value: any) {
  return typeof value?.type === 'string'
}

export function isAcertCompositeElement (value: any) {
  return typeof value?.type === 'function'
}

export function isAcertElement (value: any) {
  return isAcertHostElement(value) || isAcertCompositeElement(value)
}
