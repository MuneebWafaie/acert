import { isAcertCompositeElement, isAcertHostElement } from '../Core/utils'
import { AcertHostNode, AcertNode, AcertHostElement, HostNode, HostRenderer, AcertCompositeElement, AcertChild } from '../types'
import { CompositeInternalNode } from './CompositeInternalNode'
import { createInternalNode, InternalNode } from './InternalNode'
import { canUpdate } from './utils'

export class HostInternalNode {
  acertNode: AcertHostNode
  renderedChildren: InternalNode[] = []
  hostRenderer: HostRenderer
  eventListeners: {event:string, listener: (e: Event)=>void}[] = []
  hostNode: HostNode | null = null

  constructor (node: AcertHostNode, hostRenderer: HostRenderer) {
    this.acertNode = node
    this.hostRenderer = hostRenderer
  }

  getHostNode ():HostNode | null {
    return this.hostNode
  }

  mount ():HostNode {
    // logger(LogLevel.Info, '[mounting]', 'InternalTree/HostInternalNode.ts > HostInternalNode > mount', 'mounting', this)
    if (!isAcertHostElement(this.acertNode)) {
      this.hostNode = this.hostRenderer.createHostSubTree(this, [])
      return this.hostNode
    }

    const acertNode = this.acertNode as AcertHostElement // OGTYPE:AcertNode AS: since not AcertHostElement was already handled above
    const props = acertNode.props
    let children:AcertChild | AcertChild[] = props?.children || []
    if (!Array.isArray(children)) {
      children = [children]
    }
    const flatChildren:AcertNode[] = []
    children.forEach(child => {
      if (!Array.isArray(child)) return flatChildren.push(child)
      child.forEach(el => flatChildren.push(el))
    })

    // Create and save the contained children.
    this.renderedChildren = flatChildren.map((child) => createInternalNode(child, this.hostRenderer))

    // Collect Host nodes they return on mount
    const childHostNodes = this.renderedChildren.map(child => child.mount()).filter(childHostNode => childHostNode !== null) as HostNode[] // OGTYPE:(HostNode|null)[] AS: since filted out null elements

    // Return the Host node as mount result
    this.hostNode = this.hostRenderer.createHostSubTree(this, childHostNodes)
    return this.hostNode
  }

  receiveUpdate (nextAcertNode: AcertHostNode):void {
    // logger(LogLevel.Info, '[updating]', 'InternalTree/HostInternalNode.ts > HostInternalNode > recieveUpdate', 'updating', this, 'with', nextAcertNode)
    if (this.hostNode === null) {
      throw new Error('tried to update host node before mounting')
    }

    if (!isAcertHostElement(this.acertNode)) {
      const oldHostNode = this.hostNode
      this.acertNode = nextAcertNode

      const newHostNode = this.hostRenderer.createHostSubTree(this, [])
      this.hostNode = newHostNode
      this.hostRenderer.replaceHostSubTree(oldHostNode, newHostNode)
      return
    }

    const nextAcertElement = nextAcertNode as AcertHostElement // OGTYPE: AcertHostNode, AS: since not AcertHostElement is checked by the if above
    const prevAcertElement = this.acertNode as AcertHostElement// OGTYPE: AcertHostNode, AS: since prevAcertElement and nextAcertElement must be same type otherwise we would not have recieved the update
    const prevProps = prevAcertElement.props
    const nextProps = nextAcertElement.props

    // Remove old attributes and Set next attributes.
    this.hostRenderer.utils.removeAttributesByPropDiff(this, prevProps, nextProps)
    this.hostRenderer.utils.setAttributesFromProps(this, nextProps)

    this.acertNode = nextAcertElement

    let prevChildren = prevProps?.children || []
    if (!Array.isArray(prevChildren)) {
      prevChildren = [prevChildren]
    }
    const flatPrevChildren:AcertNode[] = []
    prevChildren.forEach(child => {
      if (!Array.isArray(child)) return flatPrevChildren.push(child)
      child.forEach(el => flatPrevChildren.push(el))
    })

    let nextChildren = nextProps?.children || []
    if (!Array.isArray(nextChildren)) {
      nextChildren = [nextChildren]
    }
    const flatNextChildren:AcertNode[] = []
    nextChildren.forEach(child => {
      if (!Array.isArray(child)) return flatNextChildren.push(child)
      child.forEach(el => flatNextChildren.push(el))
    })

    const prevRenderedChildren = this.renderedChildren
    const nextRenderedChildren: InternalNode[] = []

    const operationQueue = []
    for (let i = 0; i < flatNextChildren.length; i++) {
      const prevChildInternalNode = prevRenderedChildren[i]

      if (!prevChildInternalNode) {
        const nextChildInternalNode = createInternalNode(flatNextChildren[i], this.hostRenderer)
        const hostNode = nextChildInternalNode.mount()

        operationQueue.push({ type: 'ADD', hostNode })
        nextRenderedChildren.push(nextChildInternalNode)
        continue
      }

      if (!canUpdate(flatPrevChildren[i], flatNextChildren[i])) {
        const nextChildInternalNode = createInternalNode(flatNextChildren[i], this.hostRenderer)

        const nextHostNode = nextChildInternalNode.mount()
        const prevHostNode = prevChildInternalNode.getHostNode()
        prevChildInternalNode.unmount()

        if (prevHostNode === null) throw new Error('tried to update tree that was not fully mounted')
        nextRenderedChildren.push(nextChildInternalNode)
        operationQueue.push({ type: 'REPLACE', prevHostNode, nextHostNode })
        continue
      }

      const nextChild = flatNextChildren[i]
      nextRenderedChildren.push(prevChildInternalNode)
      if (isAcertCompositeElement(nextChild)) {
        (prevChildInternalNode as CompositeInternalNode).receiveUpdate(flatNextChildren[i] as AcertCompositeElement) // OGTYPE: InternalNode, AS: checked by if above
        continue
      }
      (prevChildInternalNode as HostInternalNode).receiveUpdate(flatNextChildren[i] as AcertHostNode) // OGTYPE: InternalNode, AS: if not CompositeInternalNode must be HostInternalNode
    }

    for (let i = flatNextChildren.length; i < flatPrevChildren.length; i++) {
      const prevChildInternalNode = prevRenderedChildren[i]
      const hostNode = prevChildInternalNode.getHostNode()
      prevChildInternalNode.unmount()

      if (hostNode === null) throw new Error('tried to update tree that was not fully mounted')
      operationQueue.push({ type: 'REMOVE', hostNode })
    }

    this.renderedChildren = nextRenderedChildren

    while (operationQueue.length > 0) {
      const operation = operationQueue.shift()
      switch (operation?.type) {
        case 'ADD':
          if (operation.hostNode) this.hostRenderer.appendHostSubTree(this.hostNode, operation.hostNode)
          break

        case 'REPLACE':
          if (operation.prevHostNode && operation.nextHostNode) this.hostRenderer.replaceHostSubTree(operation.prevHostNode, operation.nextHostNode)
          break

        case 'REMOVE':
          if (operation.hostNode) this.hostRenderer.removeHostSubTree(operation.hostNode)
          break
      }
    }
  }

  unmount () {
    // logger(LogLevel.Info, '[mounting]', 'InternalTree/HostInternalNode.ts > HostInternalNode > unmount', 'unmounting', this)
    this.eventListeners.forEach(({ event }) => this.hostRenderer.utils.clearEventListener(this, event))
    this.renderedChildren.forEach(renderedChild => renderedChild.unmount())
  }
}
