import { AcertNode, AcertCompositeElement, AcertHostNode, HostRenderer } from '../types'
import { CompositeInternalNode } from './CompositeInternalNode'
import { HostInternalNode } from './HostInternalNode'
export type InternalNode = CompositeInternalNode | HostInternalNode

export function createInternalNode (node: AcertNode, hostRenderer: HostRenderer):InternalNode {
  if (typeof node === 'object' && typeof node?.type === 'function') return new CompositeInternalNode(node as AcertCompositeElement, hostRenderer) // OGTYPE: AcertNode, AS: since its type property is a function, can only be AcertCompositeElement
  return new HostInternalNode(node as AcertHostNode, hostRenderer) // OGTYPE: AcertNode, AS: AcertNode = AcertHostNode | AcertCompositeElement, AcertCompositeElement handeled above
}
