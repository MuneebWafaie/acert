import { AcertNode, AcertElement } from '../types'
import { isAcertElement } from '../Core/utils'
export function canUpdate (prevAcertNode:AcertNode, nextAcertNode: AcertNode) {
  const bothNotAcertElement = !isAcertElement(prevAcertNode) && !isAcertElement(nextAcertNode)
  const bothAcertElements = isAcertElement(prevAcertNode) && isAcertElement(nextAcertNode)
  const sameDataType = typeof prevAcertNode === typeof nextAcertNode
  const sameAcertElementType = (prevAcertNode as AcertElement)?.type === (nextAcertNode as AcertElement)?.type // OGTYPE: AcertNode, AcertNode

  return (bothNotAcertElement && sameDataType) || (bothAcertElements && sameAcertElementType)
}
