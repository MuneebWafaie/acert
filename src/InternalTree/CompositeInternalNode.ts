import { isAcertCompositeElement } from '../Core/utils'
import { AcertCompositeElement, AcertHostNode, HostNode, HostRenderer } from '../types'
import { HostInternalNode } from './HostInternalNode'
import { createInternalNode, InternalNode } from './InternalNode'
import { canUpdate } from './utils'

type UseStatePayload<T> = [T, (newValue:T | ((oldValue: T) => T)) => void]
type UseEffectPayload = [()=>void | (()=>()=>void), (()=>void) | null, any[] | null, boolean]
type UseRefPayload<T> = {current: T}
type HookName = 'useState' | 'useEffect' | 'useRef'
type HookPayload<T> = UseStatePayload<T> | UseEffectPayload | UseRefPayload<T>

/* eslint-disable no-use-before-define */
interface HookMemoryCell<T=HookPayload<any>> {
  hook: HookName
  payload: T
  next: HookMemoryCell<any> | null
}
/* eslint-enable no-use-before-define */

/* eslint-disable no-use-before-define */
export const dispatcher:{currentDispatcher: CompositeInternalNode | null} = { currentDispatcher: null }
/* eslint-enable no-use-before-define */

export class CompositeInternalNode {
  acertNode: AcertCompositeElement
  renderedInternalNode: InternalNode | null = null
  hostRenderer: HostRenderer
  private firstHookMemoryCell: HookMemoryCell<any> | null = null
  private currentHookMemoryCell: HookMemoryCell<any> | null = null
  private mounting: boolean = false

  constructor (element: AcertCompositeElement, hostRenderer: HostRenderer) {
    this.acertNode = element
    this.hostRenderer = hostRenderer
  }

  private createCurrentHookMemeoryCell<T=HookPayload<any>> (hook: HookName, getPayload: (memoryCellRef: Partial<HookMemoryCell<T>>) => T) {
    // first hook
    const partialHookMemoryCell:Partial<HookMemoryCell<T>> = { hook, next: null }
    partialHookMemoryCell.payload = getPayload(partialHookMemoryCell)
    const hookMemoryCell = partialHookMemoryCell as HookMemoryCell<T> // OGTYPE:Partial<HookMemoryCell>, AS: not a partial anymore since payload has been added

    if (this.currentHookMemoryCell === null && this.firstHookMemoryCell === null) {
      this.firstHookMemoryCell = hookMemoryCell
      this.currentHookMemoryCell = this.firstHookMemoryCell
      return
    }

    // new hook
    if (this.currentHookMemoryCell) {
      this.currentHookMemoryCell.next = hookMemoryCell
      this.currentHookMemoryCell = this.currentHookMemoryCell.next
    }
  }

  private render () {
    dispatcher.currentDispatcher = this
    const renderedAcertNode = this.acertNode.type(this.acertNode.props)
    dispatcher.currentDispatcher = null // this is to make sure calling hooks outside render phase gives null access error
    this.currentHookMemoryCell = this.firstHookMemoryCell // reset the queue pointer back to head
    return renderedAcertNode
  }

  private runEffectsAfterPaint () {
    if (!this.firstHookMemoryCell) return
    const useEffectMemoryCells = getUseEffectMemoryCells(this.firstHookMemoryCell)
    const effectsToRun = useEffectMemoryCells.filter(useEffectMemoryCell => useEffectMemoryCell.payload[3])

    this.hostRenderer.utils.runCallbackAfterPaint(() => {
      effectsToRun.forEach(useEffectMemoryCell => {
        useEffectMemoryCell.payload[1] = useEffectMemoryCell.payload[0]() || null
      })
    })
  }

  private componentWillUnmount () {
    if (!this.firstHookMemoryCell) return
    const useEffectMemoryCells = getUseEffectMemoryCells(this.firstHookMemoryCell)
    const effectsToRun = useEffectMemoryCells.filter(useEffectMemoryCell => useEffectMemoryCell.payload[1] !== null) // get effects where cleanup function is provided

    effectsToRun.forEach(useEffectMemoryCell => {
      (useEffectMemoryCell.payload[1] as () => void)() // OGTYPE: (() => void) | null, AS: is not null gaurenteed by filter above
    })
  }

  useState<T> (initialValue: T): UseStatePayload<T> {
    // if mounting then create hook
    if (this.mounting) {
      this.createCurrentHookMemeoryCell<UseStatePayload<T>>('useState', (hookMemoryCellRef) => {
        // setValue function
        const setValue = (newValueSetter:T | ((oldValue: T) => T)) => {
          if (hookMemoryCellRef.payload === undefined) throw new Error('called setValue before mounting completed')

          // set new value
          const oldValue = hookMemoryCellRef.payload[0]
          if (typeof newValueSetter === 'function') {
            const newValue = (newValueSetter as ((oldValue: T)=>T))(oldValue) // OGTYPE: T | ((oldValue: T) => T), AS: is a function since checked by if
            hookMemoryCellRef.payload[0] = newValue
          } else {
            const newValue = newValueSetter
            hookMemoryCellRef.payload[0] = newValue
          }

          // send update to internal node
          this.receiveUpdate(this.acertNode)
        }
        return [initialValue, setValue]
      })
    }
    if (this.currentHookMemoryCell === null) throw new Error('called hook does not exist')
    if (this.currentHookMemoryCell.hook !== 'useState') throw new Error('called hook out of order')

    // return payload
    const payload = (this.currentHookMemoryCell.payload as UseStatePayload<T>) // OGTYPE: HookPayload<any>, AS: since checked by if above
    // move queue forward if not mounting, while mounting createCurrentHookMemeoryCell moves the queue forward
    if (!this.mounting) this.currentHookMemoryCell = this.currentHookMemoryCell?.next || null
    return payload
  }

  useEffect (effect: (()=>void | (()=>()=>void)), deps?: any[]) {
    if (this.mounting) {
      this.createCurrentHookMemeoryCell<UseEffectPayload>('useEffect', () => [effect, null, deps || null, true])
      return
    }
    if (this.currentHookMemoryCell === null) throw new Error('called hook does not exist')
    if (this.currentHookMemoryCell.hook !== 'useEffect') throw new Error('called hook out of order')

    const currentHookMemoryCell = this.currentHookMemoryCell as HookMemoryCell<UseEffectPayload> // OGTYPE: HookMemoryCell<any>, AS: since checked by if above

    const oldDeps = (currentHookMemoryCell.payload)[2]
    let runEffect = oldDeps === null && deps === undefined
    if (oldDeps !== null && deps !== undefined) {
      if (oldDeps.length !== deps.length) throw new Error('number of dependencies changed for useEffect')
      for (let i = 0; i < oldDeps.length; i++) {
        runEffect = !Object.is(oldDeps[i], deps[i])
        if (runEffect) break
      }
    }
    currentHookMemoryCell.payload[3] = runEffect
    currentHookMemoryCell.payload[2] = deps || null

    // move queue forward if not mounting, while mounting createCurrentHookMemeoryCell moves the queue forward
    this.currentHookMemoryCell = this.currentHookMemoryCell.next || null
  }

  useRef<T> (initialValue: T) {
    if (this.mounting) {
      this.createCurrentHookMemeoryCell<UseRefPayload<T>>('useRef', () => ({ current: initialValue }))
      return this.currentHookMemoryCell?.payload as UseRefPayload<T> // OGTYPE: any, AS: since useRef cell is created by createCurrentHookMemeoryCell
    }
    const payload = this.currentHookMemoryCell?.payload as UseRefPayload<T> // OGTYPE: any, AS: since useRef cell is created by createCurrentHookMemeoryCell
    // move queue forward if not mounting, while mounting createCurrentHookMemeoryCell moves the queue forward
    this.currentHookMemoryCell = this.currentHookMemoryCell?.next || null
    return payload
  }

  getHostNode ():HostNode | null {
    return this.renderedInternalNode?.getHostNode() || null
  }

  mount ():HostNode {
    // logger(LogLevel.Info, '[mounting]', 'InternalTree/CompositeInternalNode.ts > CompositeInternalNode > mount', 'mounting', this)
    this.mounting = true

    const renderedAcertNode = this.render()

    const renderedInternalNode = createInternalNode(renderedAcertNode, this.hostRenderer)
    this.renderedInternalNode = renderedInternalNode

    // Mount the rendered output
    const renderedHostNode = renderedInternalNode.mount()
    this.mounting = false
    this.runEffectsAfterPaint()
    return renderedHostNode
  }

  receiveUpdate (nextElement: AcertCompositeElement):void {
    // logger(LogLevel.Info, '[updating]', 'InternalTree/CompositeInternalNode.ts > CompositeInternalNode > recieveUpdate', 'updating', this, 'with', nextElement)
    // const prevProps = this.element.props
    if (this.renderedInternalNode === null) throw new Error('tried to update a component which has not been mounted yet') // not mounted yet
    this.mounting = false

    const prevRenderedInternalNode = this.renderedInternalNode
    const prevRenderedAcertNode = prevRenderedInternalNode.acertNode

    // Update *own* acertNode
    // Figure out what the next render output is
    this.acertNode = nextElement
    const nextRenderedAcertNode = this.render()

    // when prevRenderedInternalNode can be updated:
    if (canUpdate(prevRenderedAcertNode, nextRenderedAcertNode)) {
      if (!isAcertCompositeElement(prevRenderedAcertNode)) {
        (prevRenderedInternalNode as HostInternalNode).receiveUpdate(nextRenderedAcertNode as AcertHostNode) // AS:since is not AcertCompositeElement and both are same type(determined by canUpdate)
        this.runEffectsAfterPaint()
        return
      }
      (prevRenderedInternalNode as CompositeInternalNode).receiveUpdate(nextRenderedAcertNode as AcertCompositeElement)
      this.runEffectsAfterPaint()
      return
    }

    // when prevRenderedInternalNode can NOT be updated:
    const nextRenderedInternalNode = createInternalNode(nextRenderedAcertNode, this.hostRenderer)
    this.renderedInternalNode = nextRenderedInternalNode

    const prevHostNode = prevRenderedInternalNode.getHostNode()
    prevRenderedInternalNode.unmount()
    const nextHostNode = nextRenderedInternalNode.mount()
    if (prevHostNode === null) throw new Error('tried to send update before full tree was mounted')
    this.hostRenderer.replaceHostSubTree(prevHostNode, nextHostNode)
    this.runEffectsAfterPaint()
  }

  unmount () {
    // logger(LogLevel.Info, '[mounting]', 'InternalTree/CompositeInternalNode.ts > CompositeInternalNode > unmount', 'unmounting', this)
    this.componentWillUnmount()
    this.renderedInternalNode?.unmount()
  }
}

function getUseEffectMemoryCells (firstHookMemoryCell: HookMemoryCell):HookMemoryCell<UseEffectPayload>[] {
  let currentHookMemoryCell: HookMemoryCell | null = firstHookMemoryCell
  const useEffectMemoryCells = []
  while (currentHookMemoryCell !== null) {
    if (currentHookMemoryCell.hook === 'useEffect') useEffectMemoryCells.push(currentHookMemoryCell)
    currentHookMemoryCell = currentHookMemoryCell.next
  }
  return useEffectMemoryCells as HookMemoryCell<UseEffectPayload>[]
}
