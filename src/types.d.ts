import { HostInternalNode, InternalNode } from './InternalTree'

/* eslint-disable no-use-before-define */
export type Props = {
  children?: AcertChild | AcertChild[];
  [index: string]: any;
} | null
export type Component = (props: Props) => AcertNode;
export type AcertHostNode = boolean | null | undefined | number | string | AcertHostElement
export type AcertNode = AcertHostNode | AcertCompositeElement;
export type AcertChild = AcertNode | AcertNode[];

/**
 * An element describing host UI(single host platform supported component(e.g a dom node)).
 */
export interface AcertHostElement{
  type: string;
  props: Props;
}

/**
 * An element describing composite UI(made up of multiple HostElements or CompositeElement).
 */
export interface AcertCompositeElement{
  type: Component;
  props: Props;
}

/**
 * Objects that describe some UI.
 */
export type AcertElement = AcertHostElement | AcertCompositeElement

export interface HostNode{
  _internalNode?:InternalNode
}

export interface HostRendererUtils{
  setEventListener: (internalNode: HostInternalNode, event: string, listener: (e:Event)=>void) => void;
  clearEventListener: (internalNode: HostInternalNode, event: string) => void;
  setAttributesFromProps: (internalNode: HostInternalNode, props: Props) => void;
  removeAttributesByPropDiff: (internalNode: HostInternalNode, prevProps: Props, nextProps: Props) => void;
  runCallbackAfterPaint: (callback: (e:Event)=>void) => void
}

export interface HostRenderer{
  createHostSubTree: (internalNode: HostInternalNode, childHostNodes:HostNode[]) => HostNode;
  replaceHostSubTree: (OldHostSubTreeRoot: HostNode, NewHostSubTreeRoot: HostNode) => void;
  removeHostSubTree: (SubTreeRoot: HostNode) => void;
  appendHostSubTree: (parentNode: HostNode, childNode: HostNode) => void;
  utils: HostRendererUtils;
}

/* eslint-enable no-use-before-define */
