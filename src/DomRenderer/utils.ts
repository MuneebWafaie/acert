import { HostInternalNode } from '../InternalTree'
import { HostRendererUtils, Props } from '../types'

export const domRendererUtils: HostRendererUtils = {
  setEventListener: (internalNode: HostInternalNode, event: string, listener: (e:Event)=>void) => {
    // logger(LogLevel.Info, '[rendererUtils]', 'DomRenderer/utils.ts > domRendererUtils > setEventListener', 'setting listener', listener, 'for event', { event }, 'on', internalNode)
    domRendererUtils.clearEventListener(internalNode, event);
    (internalNode.hostNode as Node)?.addEventListener(event, listener) // OGTYPE: HostNode, AS: since HostNode is domNode
    internalNode.eventListeners.push({ event, listener })
  },

  clearEventListener: (internalNode: HostInternalNode, event: string) => {
    // logger(LogLevel.Info, '[rendererUtils]', 'DomRenderer/utils.ts > domRendererUtils > clearEventListener', 'clearing listener for event', { event }, 'on', internalNode)
    const listenerIdx = internalNode.eventListeners.findIndex(el => el.event === event)
    const listener = listenerIdx >= 0 ? internalNode.eventListeners[listenerIdx].listener : null
    if (listener && internalNode.hostNode) {
      (internalNode.hostNode as Node).removeEventListener(event, listener) // OGTYPE: HostNode, AS: since HostNode is domNode
      internalNode.eventListeners.splice(listenerIdx, 1)
    }
  },

  setAttributesFromProps: (internalNode: HostInternalNode, props: Props) => {
    // logger(LogLevel.Info, '[rendererUtils]', 'DomRenderer/utils.ts > domRendererUtils > setAttributesFromProps', 'setting attributes from props', props, 'on', internalNode)
    if (props === null) return
    const domNode = internalNode.hostNode as Node // OGTYPE: HostNode, AS: since HostNode is domNode

    Object.keys(props).forEach(propName => {
      if (propName !== 'children') {
        if (isEventListenerName(propName)) return domRendererUtils.setEventListener(internalNode, getEventNameFromPropName(propName), props[propName])
        if (propName === 'ref') {
          props[propName].current = domNode
          return
        }
        if (domNode.ELEMENT_NODE) { // is an element node
          (domNode as Element).setAttribute(propName, props[propName]) // OGTYPE: Node, AS: since checked by if above
        }
      }
    })
  },

  removeAttributesByPropDiff: (internalNode: HostInternalNode, prevProps:Props, nextProps: Props) => {
    // logger(LogLevel.Info, '[rendererUtils]', 'DomRenderer/utils.ts > domRendererUtils > removeAttributesByPropDiff', 'removing attributes by diffing props', nextProps, 'and', prevProps, 'on', internalNode)
    if (!prevProps) return
    nextProps = nextProps || {}
    const domNode = internalNode.hostNode as Node // OGTYPE: HostNode, AS: since HostNode is domNode

    Object.keys(prevProps).forEach(propName => {
      if (propName !== 'children' && !Object.hasOwnProperty.call(nextProps, propName)) {
        if (isEventListenerName(propName)) {
          return domRendererUtils.clearEventListener(internalNode, getEventNameFromPropName(propName))
        }
        if (domNode.ELEMENT_NODE) { // is an element node
          (domNode as Element).removeAttribute(propName) // OGTYPE: Node, AS: since checked by if above
        }
      }
    })
  },

  runCallbackAfterPaint: (callback) => {
    requestAnimationFrame(() => {
      const messageChannel = new MessageChannel()

      // Setup the callback to run in a Task
      messageChannel.port1.onmessage = callback

      // Queue the Task on the Task Queue
      messageChannel.port2.postMessage(undefined)
    })
  }
}

function isEventListenerName (propName:string) {
  const firstTwoChars = propName.slice(0, 2)
  const thirdChar = propName.slice(2, 3)
  return firstTwoChars === 'on' && thirdChar === thirdChar.toUpperCase()
}

function getEventNameFromPropName (propName:string) {
  if (!isEventListenerName(propName)) return ''
  return (propName.slice(2)).toLowerCase()
}
