import { isAcertElement } from '../Core/utils'
import { HostInternalNode, createInternalNode, canUpdate } from '../InternalTree'
import { AcertElement, AcertHostElement, AcertHostNode, AcertNode, HostNode, HostRenderer, HostRendererUtils } from '../types'
import { domRendererUtils } from './utils'

class DomRenderer implements HostRenderer {
  utils: HostRendererUtils = domRendererUtils

  createHostSubTree (internalNode: HostInternalNode, children: HostNode[]):HostNode {
    // logger(LogLevel.Info, '[renderer]', 'DomRenderer/renderer.ts > DomRenderer > createHostSubTree', 'creating dom tree for', internalNode, 'with children', children)
    const acertNode = internalNode.acertNode

    // if not element
    if (!isAcertElement(internalNode.acertNode)) {
      let domNode = document.createTextNode(String(acertNode)) as HostNode // OGTYPE: Text, AS: since text nodes are dom nodes therefore HostNodes

      if (typeof acertNode === 'boolean' || typeof acertNode === 'undefined' || acertNode === null) domNode = document.createComment(String(acertNode)) as HostNode // OGTYPE: Comment, AS: since comment nodes are dom nodes therefore HostNodes
      domNode._internalNode = internalNode
      return domNode
    }

    const acertHostElement = acertNode as AcertHostElement // OGTYPE: AcertHostNode, AS: since acertNode:AcertHostNode and not Element has already been handled above, which only leaves AcertHostElement
    const root = document.createElement(acertHostElement.type)
    internalNode.hostNode = root as HostNode // OGTYPE:HTMLElement, AS: since element nodes are dom nodes therefore HostNodes
    if (acertHostElement.props !== null) this.utils.setAttributesFromProps(internalNode, acertHostElement.props)

    children.forEach(child => root.appendChild(child as Node)); // OGTYPE:HostNode, AS: since hostNodes are dom nodes
    (root as HostNode)._internalNode = internalNode // OGTYPE:HTMLElement, AS: since element nodes are dom nodes therefore HostNodes
    return root as HostNode // OGTYPE:HTMLElement, AS: since element nodes are dom nodes therefore HostNodes
  }

  appendHostSubTree (parentNode: HostNode, childNode: HostNode) {
    // logger(LogLevel.Info, '[renderer]', 'DomRenderer/renderer.ts > DomRenderer > appendHostSubTree', 'appending', childNode, 'to', parentNode);
    (parentNode as Node).appendChild(childNode as Node) // OGTYPE: HostNode for both AS: since HostNodes are dom nodes
  }

  replaceHostSubTree (oldHostSubTreeRoot: HostNode, newHostSubTreeRoot: HostNode) {
    // logger(LogLevel.Info, '[renderer]', 'DomRenderer/renderer.ts > DomRenderer > replaceChild', 'replacing', oldHostSubTreeRoot, 'with', newHostSubTreeRoot);
    (oldHostSubTreeRoot as Node).parentNode?.replaceChild(newHostSubTreeRoot as Node, oldHostSubTreeRoot as Node) // OGTYPE: HostNode for both AS: since HostNodes are dom nodes
  }

  removeHostSubTree (subTreeRoot: HostNode) {
    // logger(LogLevel.Info, '[renderer]', 'DomRenderer/renderer.ts > DomRenderer > removeHostSubTree', 'removing', subTreeRoot);
    (subTreeRoot as Node).parentNode?.removeChild(subTreeRoot as Node) // OGTYPE: HostNode for both AS: since HostNodes are dom nodes
  }
}

function mountTree (acertNode: AcertNode, container: Node):void {
  // logger(LogLevel.Info, '[misc]', 'Renderer/renderer.ts > mountTree', 'mounting', acertNode, 'at', container)
  if (container.firstChild) {
    // logger(LogLevel.Info, '[misc]', 'Renderer/renderer.ts > mountTree', 'update tree', container.firstChild, 'with', acertNode)
    const oldInternalNode = (container.firstChild as HostNode)._internalNode as HostInternalNode // OGTYPE: ChildNode, any AS: since dom nodes are HostNodes, since internal node prop on a dom node is always a HostInternalNode
    if (canUpdate(oldInternalNode?.acertNode, acertNode)) return oldInternalNode?.receiveUpdate(acertNode as AcertHostNode) // OGTYPE: AcertNode, AS: canUpdate function determined that we can update, therefore the type of old and new acertNode is same, which means, it must be a AcertHostNode
    unmountTree(container)
  }

  const rootInternalNode = createInternalNode(acertNode, new DomRenderer())
  const rootDomNode = rootInternalNode.mount()
  rootDomNode._internalNode = rootInternalNode

  container.appendChild(rootDomNode as Node) // OGTYPE: AcertNode, AS: canUpdate function determined that we can update, therefore the type of old and new acertNode is same, which means, it must be a AcertHostNode
}

function unmountTree (container: Node) {
  // logger(LogLevel.Info, '[misc]', 'Renderer/renderer.ts > unmountTree', 'unmounting', container)
  const node = container.firstChild as HostNode // OGTYPE: ChildNode AS: since dom nodes are HostNodes
  const rootComponent = node._internalNode

  // Unmount the tree and clear the container
  rootComponent?.unmount()
  if (container.ELEMENT_NODE) {
    (container as Element).innerHTML = '' // OGTYPE: Node AS: confirmed by container.ELEMENT_NODE property
  }
}

export function render (el: AcertElement, container: Node) {
  // logger(LogLevel.Info, '[misc]', 'Renderer/renderer.ts > root.renderAtRoot', 'rendering element', el, 'at root node', rootNode)
  mountTree(el, container)
}
